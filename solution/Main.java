package seminar10.solution;

import seminar10solution.expression.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        List<Command> commandList = new ArrayList();
        commandList.add(new Assignment(new Variable("a"), new Constant(5.0)));
        commandList.add(new Assignment(new Variable("c"), new Plus(new Variable("a"), new Variable("b"))));
        commandList.add(new Print(new Variable("a")));
        commandList.add(new Print(new Variable("b")));
        commandList.add(new Print(new Variable("c")));
        commandList.add(new Print(new Times(new Variable("c"), new Variable("c"))));
        commandList.add(new Assignment(new Variable("d"), new Sinus(new Variable(("a")))));
        commandList.add(new Assignment(new Variable("a"), new Cosine(new Constant(0.0))));
        Command e = new CommandList(commandList);

        Map<String, Double> variablesContext = new HashMap();
        variablesContext.put("b", 1.0);

        e.registerObserver(new EvaluationObserver());

        e.interpret(variablesContext);
        Iterator iterator = variablesContext.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry pair = (Map.Entry) iterator.next();
            System.out.println(pair.getKey() + "=" + pair.getValue());
        }

    }
}