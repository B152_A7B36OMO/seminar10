package seminar10.solution.expression;


import java.util.Map;

public class Print extends Command {
    private final Expression expression;

    public Print(Expression expression) {
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }

    public void interpret(Map<String, Double> context) {
        notifyObservers();
    }
}