package seminar10.solution.expression;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommandList extends Command {
    private final ImmutableList<Command> commands;
    private ArrayList<IObserver> observers;

    public CommandList(List<Command> commands) {
        this.commands = ImmutableList.copyOf(commands);
        this.observers = new ArrayList<IObserver>();
    }

    public ImmutableList<Command> getCommands() {
        return commands;
    }

    public void interpret(Map<String, Double> context) {
        notifyObservers();
        for (Command command : commands){
            command.interpret(context);
        }
    }

    public void registerObserver(IObserver observer) {
        for (Command command : commands){
            command.registerObserver(observer);
        }
        observers.add(observer);
    }

    public void unregisterObserver(IObserver observer) {
        for (Command command : commands){
            command.unregisterObserver(observer);
        }
        observers.remove(observer);
    }

    public void notifyObservers() {
        for(IObserver observer : observers){
            observer.notification(this);
        }
    }
}
