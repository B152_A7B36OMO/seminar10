package seminar10.solution.expression;

import java.util.Map;

public class Variable extends Expression {
    private final String name;

    public Variable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double interpret(Map<String, Double> context) {
        notifyObservers();
        return context.get(name);
    }
}
