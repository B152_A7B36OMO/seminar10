package seminar10.solution.expression;

import java.util.Map;

public class Sinus extends Expression {
    private final Expression argument;

    public Sinus(Expression argument) {
        this.argument = argument;
    }

    public Expression getArgument() {
        return argument;
    }

    public double interpret(Map<String, Double> context) {
        notifyObservers();
        return Math.sin(argument.interpret(context));
    }

    @Override
    public void registerObserver(IObserver observer) {
        super.registerObserver(observer);
        argument.registerObserver(observer);
    }

    @Override
    public void unregisterObserver(IObserver observer) {
        super.unregisterObserver(observer);
        argument.unregisterObserver(observer);
    }
}
