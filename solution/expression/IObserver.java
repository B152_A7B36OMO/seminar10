package seminar10.solution.expression;

public interface IObserver {
    void notification(Expression source);
    void notification(Command source);
}
