package seminar10.solution.expression;

import java.util.Map;

public class Constant extends Expression {
    private final double value;

    public Constant(double value) {
        this.value = value;
    }

    public double interpret(Map<String, Double> context) {
        notifyObservers();
        return value;
    }
}

