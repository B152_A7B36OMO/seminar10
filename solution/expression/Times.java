package seminar10.solution.expression;

import java.util.Map;

public class Times extends Expression {
    private final Expression left;
    private final Expression right;

    public Times(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public Expression getLeft() {
        return left;
    }

    public Expression getRight() {
        return right;
    }

    public double interpret(Map<String, Double> context) {
        notifyObservers();
        return left.interpret(context) * right.interpret(context);
    }

    @Override
    public void registerObserver(IObserver observer) {
        super.registerObserver(observer);
        left.registerObserver(observer);
        right.registerObserver(observer);
    }

    @Override
    public void unregisterObserver(IObserver observer) {
        super.unregisterObserver(observer);
        left.unregisterObserver(observer);
        right.unregisterObserver(observer);
    }
}