package seminar10.solution.expression;

import java.util.ArrayList;
import java.util.Map;

public abstract class Expression {
    protected ArrayList<IObserver> observers;

    protected Expression() {
        this.observers = new ArrayList<IObserver>();
    }

    abstract double interpret(Map<String, Double> context);

    public void registerObserver(IObserver observer){
        observers.add(observer);
    }

    public void unregisterObserver(IObserver observer){
        observers.remove(observer);
    }

    public void notifyObservers(){
         for(IObserver observer : observers){
             observer.notification(this);
         }
    }
}
