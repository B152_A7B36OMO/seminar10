package seminar10.solution.expression;

import java.util.Map;

public class Assignment extends Command {
    private final Variable variable;
    private final Expression expression;

    public Assignment(Variable variable, Expression expression) {
        this.variable = variable;
        this.expression = expression;
    }

    public Variable getVariable() {
        return variable;
    }

    public Expression getExpression() {
        return expression;
    }


    public void interpret(Map<String, Double> context) {
        notifyObservers();
        context.put(variable.getName(), expression.interpret(context));
    }

    public void registerObserver(IObserver observer) {
           observers.add(observer);
           expression.registerObserver(observer);
    }

    public void unregisterObserver(IObserver observer) {
          observers.remove(observer);
          expression.unregisterObserver(observer);
    }

}
