package seminar10.solution;


import seminar10solution.expression.Command;
import seminar10solution.expression.Expression;
import seminar10solution.expression.IObserver;

public class EvaluationObserver implements IObserver {
    public void notification(Expression source) {
        System.out.println("Vyhodnocování expression");
    }

    public void notification(Command source) {
        System.out.println("Vyhodnocování command");
    }
}
