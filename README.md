# Zadání
1. Implementujte interpreter, který umožní vyhodnotit výraz zadaný v Main.main za předpokladu, že b = 1.
2. Implementujte observer, který při každém vyhodnocení Expression vypíše do konzole upozornění "Expression evaluated!"
3. Zařiďte, aby se kromě upozornění vypsal i typ Expression, které se vyhodnocuje.

# Pozor
Pro hladký běh programu je třeba stáhnout a do projektu přidat knihovnu [Guava](http://search.maven.org/remotecontent?filepath=com/google/guava/guava/16.0.1/guava-16.0.1.jar).