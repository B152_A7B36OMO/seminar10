package seminar10;

import seminar10.expression.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        List<Command> commandList = new ArrayList();
        commandList.add(new Assignment(new Variable("a"), new Constant(5.0)));
        commandList.add(new Assignment(new Variable("c"), new Plus(new Variable("a"), new Variable("b"))));
        commandList.add(new Print(new Variable("a")));
        commandList.add(new Print(new Variable("b")));
        commandList.add(new Print(new Variable("c")));
        commandList.add(new Print(new Times(new Variable("c"), new Variable("c"))));
        commandList.add(new Assignment(new Variable("d"), new Sinus(new Variable(("a")))));
        commandList.add(new Assignment(new Variable("a"), new Cosine(new Constant(0.0))));
        Command e = new CommandList(commandList);

        //...
    }
}