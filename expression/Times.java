package seminar10.expression;

public class Times extends Expression {
    private final Expression left;
    private final Expression right;

    public Times(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public Expression getLeft() {
        return left;
    }

    public Expression getRight() {
        return right;
    }


}