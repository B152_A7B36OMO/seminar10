package seminar10.expression;


public class Print extends Command {
    private final Expression expression;

    public Print(Expression expression) {
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }


}