package seminar10.expression;

public class Cosine extends Expression {
    private final Expression argument;

    public Cosine(Expression argument) {

        this.argument = argument;
    }

    public Expression getArgument() {
            return argument;
        }


}
