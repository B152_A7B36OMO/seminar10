package seminar10.expression;

import com.google.common.collect.ImmutableList;

import java.util.List;

public class CommandList extends Command {
    private final ImmutableList<Command> commands;


    public CommandList(List<Command> commands) {
        this.commands = ImmutableList.copyOf(commands);

    }

    public ImmutableList<Command> getCommands() {
        return commands;
    }

}
