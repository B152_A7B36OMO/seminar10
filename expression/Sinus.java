package seminar10.expression;

public class Sinus extends Expression  {
    private final Expression argument;

    public Sinus(Expression argument) {
        this.argument = argument;
    }

    public Expression getArgument() {
        return argument;
    }



}
